FROM sosinnmi2/coronamap:1.0

LABEL maintainer "sosinnmi2 <sosinnmi@naver.com>"

# 기존 /home/corona_dir 안의 내용을 모드 삭제
RUN rm -rf /home/corona_dir/*

WORKDIR /home/corona_dir

COPY . ./

CMD ["/bin/sh", "start.sh"]