[[_TOC_]]

# 목적

일일 코로나19 확진자 수를 전국의 광역시·도별 지도에 색깔의 진하기로 나타낸 지도를 만들고자 했다.   
이 지도를 도커 컨테이너로 만들기 위한 이미지 빌드 작업도 함께 들어있다.   

![map](/uploads/a5d16e863e222a7e3c80b4dbb7639172/map.png)

# 과정
1. 지도 데이터를 가공 json형식으로 추출
2. Python으로 [공공데이터 포털](https://www.data.go.kr/data/15043378/openapi.do)의 open API를 통해 확진자 수 다운로드
3. 지도 데이터와 확진자 수를 매칭시키는 [python 코드](https://gitlab.com/jonghechoi/coronamap-project/-/blob/master/corona19_showindow.py) 작성
4. Flask를 이용하여 html 파일 서비스

